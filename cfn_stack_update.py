# Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
# except in compliance with the License. A copy of the License is located at
#
#     http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is distributed on an "AS IS"
# BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under the License.
"""
A Bitbucket Builds template for updating a CloudFormation stack
joshcb@amazon.com
v1.0.0
"""
from __future__ import print_function
import sys
import argparse
import boto3
import json
from botocore.exceptions import ClientError, WaiterError
def create_stack(client, stack_name, template):
    try:
        """
        see all parameters here:
        http://boto3.readthedocs.io/en/latest/reference/services/cloudformation.html#CloudFormation.Client.update_stack
        """
        client.create_stack(
            StackName='VPCTemplate',
            TemplateBody=open(template, 'r').read(),
            Capabilities=['CAPABILITY_IAM'],
            # Required if parameters need to be entered
            # Parameters=[
            #     {
            #         'ParameterKey': 'string',
            #         'ParameterValue': 'string'
            #     },
            # ]
        )
    except ClientError as err:
        print("Failed to Create the stack.\n" + str(err))
        return False
    except IOError as err:
        print("Failed to access " + template + ".\n" + str(err))
        return False
def main():
    " Your favorite wrapper's favorite wrapper "
    parser = argparse.ArgumentParser(prog='VPCTemplate')
    parser.add_argument("stack_name", help="The CFN stack to create")
    parser.add_argument("template", help="The CFN Template to create the stack")
    args = parser.parse_args()
    try:
        client = boto3.client('cloudformation')
        #stack = cloudformation.Stack('VPCTemplate')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False

    if not create_stack(client, args.stack_name, args.template):
        sys.exit(1)
		
if __name__ == "__main__":
    main()